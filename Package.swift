// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FlomniChat",
    defaultLocalization: "ru",
    platforms: [.iOS(.v15), .macCatalyst(.v15), .macOS(.v13), .tvOS(.v15), .watchOS(.v8)],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "FlomniChat",
            targets: ["FlomniChat", "FlomniChatSDK"]),
    ],
    dependencies: [
       
        .package(url: "https://github.com/socketio/socket.io-client-swift", from: "16.0.0"),
    ],
    targets: [
        .target(
            name: "FlomniChat",
            dependencies: [
                "FlomniChatSDK",
                .product(name: "SocketIO",
                         package: "socket.io-client-swift")
            ],
           
            resources: [
                .process("Resources/Media.xcassets"),
                .process("Resources/Sounds")
            ],
            linkerSettings: [
//                .linkedFramework("FlomniChatSDK", .when(platforms: [.macCatalyst, .macOS]))
                // .linkedFramework("FlomniChatSDK")
            ]
        ),
        .binaryTarget(
            name: "FlomniChatSDK",
            url: "https://github.com/NSFuntik/flomnichatsdkzip/raw/main/FlomniChatSDK.xcframework.zip",
            checksum: "eb6264f9b0f97904f69ceb36cb3815dc27e5aea02018c60aac4c219d6146d338"
        ),
        
//        .binaryTarget(
//            name: "FlomniChatSDK",
//            path: "./Frameworks/FlomniChatSDK.xcframework") // Убедитесь, что путь правильный
//        .binaryTarget(
//            name: "SocketIO",
//            path: "Library/SocketIO.xcframework")
//        .binaryTarget(
//            name: "Starscream",
//            path: "Library/Starscream.xcframework")
    ]
)
