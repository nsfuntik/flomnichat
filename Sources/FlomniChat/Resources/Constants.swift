//
//  Constants.swift
//  FlomniChatApp
//
//  Created by Dmitry Mikhaylov on 27.02.2024.
//

import SwiftUI
@available(iOS 15.0, *)
enum SettingsLocaleString {
    public static let appName: LocalizedStringKey = "app_name"
    public static let startWebChat: LocalizedStringKey = "start_web_chat"
    public static let startNativeChat: LocalizedStringKey = "start_native_chat"
    public static let chat: LocalizedStringKey = "chat"
    public static let yourCompanyId: LocalizedStringKey = "your_company_id"
    public static let settings: LocalizedStringKey = "settings"
    public static let generalColors: LocalizedStringKey = "general_colors"
    public static let chatBgColor: LocalizedStringKey = "chat_bg_color"
    public static let sendButtonColor: LocalizedStringKey = "send_button_color"
    public static let inputTextColor: LocalizedStringKey = "input_text_color"
    public static let inputHintColor: LocalizedStringKey = "input_hint_color"
    public static let messageColors: LocalizedStringKey = "message_colors"
    public static let inMessageHintColor: LocalizedStringKey = "in_message_hint_color"
    public static let outMessageTextColor: LocalizedStringKey = "out_message_text_color"
    public static let inMessageBgColor: LocalizedStringKey = "in_message_bg_color"
    public static let outMessageBgColor: LocalizedStringKey = "out_message_bg_color"
    public static let messageDateColor: LocalizedStringKey = "message_date_color"
    public static let messageButtonColors: LocalizedStringKey = "message_button_colors"
    public static let messageButtonBgColor: LocalizedStringKey = "message_button_bg_color"
    public static let messageButtonTextColor: LocalizedStringKey = "message_button_text_color"
    public static let messageButtonPressedBgColor: LocalizedStringKey = "message_button_pressed_bg_color"
    public static let messageButtonPressedTextColor: LocalizedStringKey = "message_button_pressed_text_color"
    public static let save: LocalizedStringKey = "save"
    public static let colorHint: LocalizedStringKey = "color_hint"
    public static let messageAvatarColor: LocalizedStringKey = "message_avatar_color"
    public static let sendingAttachments: LocalizedStringKey = "sending_attachments"
    public static let disableChooseFile: LocalizedStringKey = "disable_choose_file"
    public static let disableOpenGallery: LocalizedStringKey = "disable_open_gallery"
    public static let disableTakePhoto: LocalizedStringKey = "disable_take_photo"
    public static let disableSendingFiles: LocalizedStringKey = "disable_sending_files"
    public static let disableLibraryPush: LocalizedStringKey = "disable_library_push"
}
@available(iOS 15.0, *)
#Preview(body: {
    AppSettingsLocaleStringsList()
})
@available(iOS 15.0, *)
struct AppSettingsLocaleStringsList: View {
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 10) {
                Text(SettingsLocaleString.appName, bundle: .main)
                Text(SettingsLocaleString.startWebChat, bundle: .main)
                Text(SettingsLocaleString.startNativeChat, bundle: .main)
                Text(SettingsLocaleString.chat, bundle: .main)
                Text(SettingsLocaleString.yourCompanyId, bundle: .main)
                Text(SettingsLocaleString.settings, bundle: .main)
                Text(SettingsLocaleString.generalColors, bundle: .main)
                Text(SettingsLocaleString.chatBgColor, bundle: .main)
                Text(SettingsLocaleString.sendButtonColor, bundle: .main)
                Text(SettingsLocaleString.inputTextColor, bundle: .main)
                Text(SettingsLocaleString.inputHintColor, bundle: .main)
                Text(SettingsLocaleString.messageColors, bundle: .main)
                Text(SettingsLocaleString.inMessageHintColor, bundle: .main)
                Text(SettingsLocaleString.outMessageTextColor, bundle: .main)
                Text(SettingsLocaleString.inMessageBgColor, bundle: .main)
                Text(SettingsLocaleString.outMessageBgColor, bundle: .main)
                Text(SettingsLocaleString.messageDateColor, bundle: .main)
                Text(SettingsLocaleString.messageButtonColors, bundle: .main)
                Text(SettingsLocaleString.messageButtonBgColor, bundle: .main)
                Text(SettingsLocaleString.messageButtonTextColor, bundle: .main)
                Text(SettingsLocaleString.messageButtonPressedBgColor, bundle: .main)
                Text(SettingsLocaleString.messageButtonPressedTextColor, bundle: .main)
                Text(SettingsLocaleString.save, bundle: .main)
                Text(SettingsLocaleString.colorHint, bundle: .main)
                Text(SettingsLocaleString.messageAvatarColor, bundle: .main)
                Text(SettingsLocaleString.sendingAttachments, bundle: .main)
                Text(SettingsLocaleString.disableChooseFile, bundle: .main)
                Text(SettingsLocaleString.disableOpenGallery, bundle: .main)
                Text(SettingsLocaleString.disableTakePhoto, bundle: .main)
                Text(SettingsLocaleString.disableSendingFiles, bundle: .main)
                Text(SettingsLocaleString.disableLibraryPush, bundle: .main)
            }
        }
    }
}
