//
//  SwiftUIView.swift
//
//
//  Created by Dmitry Mikhaylov on 24.04.2024.
//
import FlomniChatSDK
import SwiftUI

@available(iOS 15.0, *)
public struct TerminalTab: View {
    typealias Scope = FlomniChatSDK.SUILogScope
    @SceneStorage("currentLogsTab") var currentPageIndex: Scope = .global
    @Injected(\.chatTheme) var chatTheme
    @Namespace private var animation
    @Injected(\.global) public var global
    @Injected(\.ui) public var ui
    @Injected(\.keychain) public var keychain
    @Injected(\.socket) public var socket
    @Injected(\.network) public var network

    @State private var animationProgress: CGFloat = 0
    @State private var scrollableTabOffset: CGFloat = 0
    @State private var initialOffset: CGFloat = 0

    public init() { }

    public var body: some View {
        VStack(content: {
            switch currentPageIndex {
            case .global:
                SUILogTrace(logs: global.logs.reversed()).tag("global")
            case .ui:
                SUILogTrace(logs: ui.logs.reversed()).tag("ui")
            case .keychain:
                SUILogTrace(logs: keychain.logs.reversed()).tag("keychain")
            case .socket:
                SUILogTrace(logs: socket.logs.reversed()).tag("socket")
            case .network:
                SUILogTrace(logs: network.logs.reversed()).tag("network")
            @unknown default:
                ContentUnavailableView(
                    "@unknown default",
                    symbol: .mailAndTextMagnifyingglass,
                    description: "Please, report this bug to the developer")
            }
        })
        .padding(6)

        .patternBackground()
        .navigationTitle(currentPageIndex.rawObjectName)
        .overlay(alignment: .top, content: {
            Label(
                title: { Text(currentPageIndex.rawObjectName) },
                icon: { currentPageIndex.symbol.symbolRenderingMode(.multicolor) }
            )
            .shadow(.sticker).imageScale(.large)
            .font(.title.bold().monospaced())
            .highlightEffect()
            .padding(12)
            .spacing()
            .frame(height: 44, alignment: .bottomTrailing)
            .materialBackground(
                with: .prominent,
                blur: 6,
                clipped: Capsule(style: .continuous),
                filled: Color("ChatBG", bundle: .module).opacity(0.33),
                bordered: Color(.separator),
                width: 0.13
            )
        })
        .overlay(alignment: .bottom) {
            TabBar
        }
    }

    var TabBar: some View {
        HStack {
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 10) {
                    ForEach(
                        SUILogScope.allCases.compactMap({ ($0, currentPageIndex == $0) }),
                        id: \.0.id
                    ) { item, isSelected in
                        Button {
                            withAnimation(.bouncy(duration: 0.3)) {
                                currentPageIndex = item
                                animationProgress = 1.0
                            }
                        } label: {
                            tabIcon(item, isSelected)
                                .id(item.rawObjectName)
                                .submitScope()
                        }
                    }

                }.padding(.horizontal, 16)
            }
            .tint(Color(.secondaryLabel))
            .materialBackground(
                with: .systemMaterial,
                blur: 3,
                clipped: Capsule(style: .continuous),
                filled: Color(.secondarySystemBackground).opacity(0.13),
                bordered: Color(.separator),
                width: 0.13
            )

            .checkAnimationEnded(for: animationProgress) {
                /// Reseting to default when the animation finished
                animationProgress = 0
            }
        }
        .padding(8, 16)
        .animation(.bouncy, value: currentPageIndex)
    }

    func tabIcon(_ item: SUILogScope, _ isSelected: Bool) -> some View {
        VStack(alignment: .center, spacing: 2, content: {
            item.symbol
                .symbolRenderingMode(isSelected ? .multicolor : .hierarchical)
                .imageScale(.large)
                .font(.headline.weight(isSelected ? .medium : .regular))
                .padding(6, 8)
                .background(alignment: .center) {
                    Capsule(style: .circular)
                        .tint(isSelected ? Color(.secondarySystemFill).opacity(0.66) : .clear)
                        .if(isSelected, {
                            $0.matchedGeometryEffect(id: "activelog", in: animation)
                        })
                }
            Text(item.rawObjectName).font(.caption).foregroundStyle(.secondary)
        })

        .padding(6, 8)
        .clipped()
    }
}

@available(iOS 15.0, *)
public struct SUILogTrace: View {
    var logs: [SUILog.Event]

    @ViewBuilder
    public var body: some View {
        ScrollView {
            VStack(spacing: 8) {
                Spacer().frame(box: 44)
                ForEach(logs.indices, id: \.self) { index in

                    LogRow(logs[safe: index])
                        .clipped()
                        .background(.regularMaterial, in: RoundedRectangle(cornerRadius: 13))

                    Divider()
                }

            }.padding(4)
            Spacer(minLength: 66)
        }.ignoresSafeArea(.all, edges: .trailing).clipped()
    }

    @ViewBuilder
    func LogRow(_ event: LogEvent?) -> some View {
        if let event = event {
            DroppableView(
                thumbnail: DroppableView.Thumbnail(content: {
                    LogEventView(event: event, open: false)
                }),
                expanded: DroppableView.Expanded(content: {
                    LogEventView(event: event, open: true)

                }),
                thumbnailViewBackgroundColor: .clear,
                expandedViewBackgroundColor: .purple.opacity(0.06), textToCopy: event.message
            ).compositingGroup()
                .textSelection(.enabled).clipped().shadow(.badge)
        }
    }

    func LogEventView(event: SUILog.Event, open: Bool) -> some View {
        LazyVStack(alignment: .leading, spacing: 8) {
            HStack(spacing: 8) {
                Label(
                    "\(event.metadata.file.lastPathComponent) \u{0040} **line: \(event.metadata.line)**",
                    systemImage: "calendar.day.timeline.left"
                )
                .symbolRenderingMode(.multicolor).imageScale(.large)
                Spacer()
                Text(event.dateCreated.formatted(date: .omitted, time: .shortened))
            }
            .font(.caption2.monospaced()).lineLimit(1).truncationMode(.head)

            Divider().frame(height: 0.66).background(event.level.color).cornerRadius(100)
            HStack(spacing: 1, content: {
                event.level.emoji.symbolRenderingMode(.multicolor)
                Text("\u{25b8} \(event.message)")
                    .multilineTextAlignment(.leading)
                    .imageScale(.large)
                    .font(.footnote.monospaced().weight(.thin))
                    .lineLimit(open ? nil : 1)

                Spacer()
            })
            .padding(8)
            .background(
                Rectangle().fill(event.level.color).opacity(0.0666)).padding(-8).padding(.horizontal, -3)
            if let error = event.error {
                Divider()
                Text("🚨" + error.localizedDescription)
                    .font(.callout.monospaced().weight(.thin))
            }
        }
        .padding(8)

        .padding(.horizontal, 4)
        .clipShape(RoundedRectangle(cornerRadius: 6, style: .continuous), style: .init(eoFill: true, antialiased: true))
    }
}

