import FlomniChatSDK
import SwiftUI

public typealias Flomni = FlomniChatSDK.MainFlomniChatSDK

/// Main interface to the SwiftUI SDK.
///
/// Provides context for the views and view models. Must be initialized with a `ChatClient` on app start.
public extension MainFlomniChatSDK {
    convenience init(
        apiKey: String,
        appGroup: String,
        userId: String,
        theme: ChatTheme = ChatTheme.default
        //        appearance: Appearance = Appearance(),
    ) {
        self.init(
            chatClient: .init(
                config: FlomniChatSDK.ChatClientConfig(
                    apiKey: APIKey(apiKey),
                    applicationGroupIdentifier: appGroup,
                    user: UserCredentials(id: userId)
                )
            ), theme: theme)
    }
}

public typealias ChannelID = AnyHashable

public typealias ChatTheme = FlomniChatSDK.ChatTheme

public typealias APIKey = FlomniChatSDK.APIKey

public typealias UserCredentials = FlomniChatSDK.UserCredentials

#Preview(body: {
    TabView {
        TerminalTab().tabItem { Label("Terminal", symbol: .terminalFill) }

        MainFlomniChatSDK.ChatContentView(
            theme: ChatTheme.default,
            route: [ChannelID](),
            placement: ToolbarItemPlacement.automatic, content: {
                Spacer()
            })
            .tabItem { Label("Chats", symbol: .bubbleLeftAndBubbleRight) }

        NotificationsList(userNotificationCenter: UNUserNotificationCenter.current())
            .tabItem { Label("Notifications", symbol: .bellFill) }
    }
    .onAppear(perform: {
        _ = MainFlomniChatSDK(
            apiKey: "5d0cd1707741de0009e061cb",
            appGroup: "group.com.flomni.chat",
            userId: "c79f58b0-527d-4e75-ba58-6fbc32c6c81a",
            theme: .default
        )
    })
})
