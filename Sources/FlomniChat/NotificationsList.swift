//
//  File.swift
//  
//
//  Created by Dmitry Mikhaylov on 21.05.2024.
//

import FlomniChatSDK
import SwiftUI

public typealias NotificationsList = FlomniChatSDK.LocalNotificationList
public typealias NotificationEditor = FlomniChatSDK.LocalNotificationEditor
public typealias NotificationsHandler = FlomniChatSDK.NotificationsHandler
@available(iOS 14.0, *)
#Preview(body: {
    NotificationsList(userNotificationCenter: UNUserNotificationCenter.current())
})

