Pod::Spec.new do |s|
    s.name         = "FlomniChat"
    s.summary      = "FlomniChat: FlomniChatSDK"
    s.description  = "FlomniChat FlomniChat: FlomniChatSDK"
    s.homepage     = "https://gitlab.com/nsfuntik/flomnichat.git"
    s.license = { :type => "MIT", :file => "LICENSE" }
    s.author             = { "NSFuntik" => "nsfuntik@gmail.com" }
    s.source       = { :git => "https://gitlab.com/nsfuntik/flomnichat.git", :branch => "XCFramework" }
    s.vendored_frameworks = "FlomniChatSDK.xcframework"
    s.platform = :ios
    s.swift_version = "5.9"
    s.ios.deployment_target  = '15.0'
    s.requires_arc = true
end